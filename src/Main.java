import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Main implements KeyListener {

    private static final int mCanvasWidth = 500;
    private static final int mCanvasHeight = 500;

    private static final Color mFoodColor = new Color(0xE16135);
    private static final Color mBackgroundColor = new Color(0xB5E135);

    private static final int mSnackSpeed = 15;
    private static final Color mSnackColor = new Color(0x1F203A);


    private static final int mSnackXCellSize = 25;
    private static final int mSnackYCellSize = 25;

    private static final int mProximity = mSnackXCellSize;
    private static final int mHalfProximity = 1;

    private Image mAppleImage;

    private int mSnackXDirection = 1;
    private int mSnackYDirection = 0;

    private int mFoodX = 0;
    private int mFoodY = 0;

    public ArrayList<SnackCell> mSnackCells;

    private JFrame mJFrame;
    private Canvas mCanvas;
    private Graphics mCanvasGraphics;

    private Random mRandomGenerator;

    public Main(){
        mJFrame = new JFrame("Snack");
        mCanvas = new Canvas();

        mCanvas.setSize(mCanvasWidth, mCanvasHeight);
        mCanvas.setBackground(mBackgroundColor);

        mSnackCells = new ArrayList<>(1);

        try {
            mAppleImage = ImageIO.read(new File("apple.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        mRandomGenerator = new Random();
    }

    public void start(){
        mJFrame.setSize(mCanvasWidth, mCanvasHeight);
        mJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mJFrame.addKeyListener(this);

        mJFrame.add(mCanvas);

        mJFrame.setVisible(true);

        mSnackCells.add(new SnackCell(mCanvasWidth / 2, mCanvasHeight / 2));

        run();
    }

    private void run(){
        new Thread(() -> {
            mCanvasGraphics = mCanvas.getGraphics();

            addFood();

            while(true){
                updateView();
            }
        }).start();
    }

    private void updateView(){
        drawCanvas(mSnackXDirection * mSnackSpeed, mSnackYDirection * mSnackSpeed);
        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void addFood(){
        mFoodX = mRandomGenerator.nextInt(mCanvasWidth - 2 * mSnackXCellSize) + mSnackXCellSize;
        mFoodY = mRandomGenerator.nextInt(mCanvasHeight - 2 * mSnackYCellSize) + mSnackYCellSize;

        mCanvasGraphics.setColor(mFoodColor);
        mCanvasGraphics.fillRect(mFoodX, mFoodY, mSnackXCellSize, mSnackYCellSize);

        /*
        mCanvasGraphics.drawImage(mAppleImage, mFoodX, mFoodY, mSnackXCellSize, mSnackYCellSize, new ImageObserver() {
            @Override
            public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                return false;
            }
        });
        */
    }

    public void eatFood(){
        mCanvasGraphics.clearRect(mFoodX, mFoodY, mSnackXCellSize, mSnackYCellSize);
        addFood();
    }

    public void drawCanvas(int Xspeed, int Yspeed){
        for (int j = 0; j < mSnackCells.size(); j++) {
            SnackCell snackCell = mSnackCells.get(j);
           mCanvasGraphics.clearRect(snackCell.mX, snackCell.mY, mSnackXCellSize, mSnackYCellSize);
            snackCell.mX += Xspeed;
            snackCell.mY += Yspeed;

            if(
                    snackCell.mX < 0 ||
                    snackCell.mY < 0 ||
                    snackCell.mX + mSnackXCellSize >= mCanvasWidth ||
                    snackCell.mY + mSnackYCellSize >= mCanvasHeight
                ){
                endGame();
            }

            if(didTheSnakeTouchThisPoint(snackCell, mFoodX, mFoodY)) {
                eatFood();
            }

            mCanvasGraphics.setColor(mSnackColor);
            mCanvasGraphics.fillRect(snackCell.mX, snackCell.mY, mSnackXCellSize, mSnackYCellSize);
        }
    }

    private boolean didTheSnakeTouchThisPoint(SnackCell SnackCell, int X, int Y){
        int xDiffrent = Math.abs(SnackCell.mX - X),
            yDiffrent = Math.abs(SnackCell.mY - Y),
            x1Diffrent = Math.abs((SnackCell.mX + mSnackXCellSize) - (X + mSnackXCellSize)),
            y1Diffrent = Math.abs((SnackCell.mY + mSnackYCellSize) - (Y + mSnackYCellSize));

        if((xDiffrent < mHalfProximity || x1Diffrent < mProximity) && (yDiffrent < mHalfProximity || y1Diffrent < mProximity)){
            return true;
        }

        return false;
    }

    public static void main(String[] args){
        new Main().start();
    }

    public void endGame(){
        System.exit(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()){
            case KeyEvent.VK_DOWN:
                mSnackXDirection = 0;
                mSnackYDirection = 1;
                break;
            case KeyEvent.VK_UP:
                mSnackXDirection = 0;
                mSnackYDirection = -1;
                break;
            case KeyEvent.VK_LEFT:
                mSnackXDirection = -1;
                mSnackYDirection = 0;
                break;
            case KeyEvent.VK_RIGHT:
                mSnackXDirection = 1;
                mSnackYDirection = 0;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    class SnackCell {
        int mX;
        int mY;

        public SnackCell(int X, int Y){
            this.mX = X;
            this.mY = Y;

        }

    }

}